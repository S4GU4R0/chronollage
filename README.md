Free Palestine: I do not support the Israeli genocide in Palestine and actively work towards ending it. You can help at <https://nabali.org>.

# chronollage

## What is Chronollage

"Chronollage" is a play on the words chronos and collage. 

It is an open source alternative to services like Skedpal, Reclaim, and Motion, which are tools that integrate tasks/habit into your calendar. 

These services are great, but insufficient for my needs. They either are too expensive or the integrations are too limited. My hope is to build this out and put an affordable version on something like PikaPods while making a version free for people to host for themselves. But first, to build it. 😅

## Development

### Getting started

- Clone the project.
- Use pnpm or another package manager of your choice to install dependencies: `pnpm i`.
- Run the development server: `pnpm run dev`.

### Secrets

Needed variables are set up in `.env.example`.

- `NEXTAUTH_SECRET` needs to be set to a random string of your choice. 
You can run `openssl rand -base64 32` in the terminal to get a random string to put there.
- `NEXTAUTH_SECRET` needs to be set to your project's url. If developing, it'll be `http://localhost:3000`.
- `APPWRITE_KEY` needs to be set to an API key from an <appwrite.io> project. (they have a free tier and can be self-hosted)

### Integrations

#### Google 

##### Step 1. Set up a Google Cloud project

<https://console.cloud.google.com/projectcreate>

##### Step 2: Create Access Credentials

<https://console.cloud.google.com/apis/credentials/oauthclient>

1. Web application is the type.
2. Choose a name you want.
3. Add URIs for requests (e.g. http://localhost:3000) and redirects (e.g. http://localhost:3000/api/auth/callback/google).
4. Click Create.
5. Download the JSON file.

#### Google Tasks

##### Step 1: Enable the Tasks API

<https://console.cloud.google.com/apis/library/tasks.googleapis.com>

##### Step 2: Configure OAuth consent

Users need to login to their Google accounts in order to link things together. 
<https://console.cloud.google.com/apis/credentials/consent>

1. Select external
2. Fill out the form. Feel free to skip optional fields.
3. Add a scope: Search "Google Tasks API" and check the first one. Click update.
4. Click save and continue.





## License

TBD

