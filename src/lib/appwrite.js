const sdk = require('node-appwrite');

const appwriteConnection = new sdk.Client()
    .setEndpoint('https://cloud.appwrite.io/v1') // Your API Endpoint
    .setProject('66713462003640551e64') // Your project ID
    .setKey(process.env.APPWRITE_KEY); // Your secret API key

const usersConnection = new sdk.Users(appwriteConnection);
const databases = new sdk.Databases(appwriteConnection);


export {
    appwriteConnection,
    usersConnection,
    sdk,
    databases
}