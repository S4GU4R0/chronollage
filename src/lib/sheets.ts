import { GoogleSpreadsheet } from "google-spreadsheet";
import { JWT } from "google-auth-library";

const SCOPES = ["https://www.googleapis.com/auth/spreadsheets"];

const jwtFromEnv = new JWT({
  email: process.env.CLIENT_EMAIL,
  key: process.env.PRIVATE_KEY.replace(/\\n/g, '\n'),
  scopes: SCOPES,
});

// Initialize auth - see https://theoephraim.github.io/node-google-spreadsheet/#/guides/authentication
const doc = new GoogleSpreadsheet(
  "1XGBG9i8w-WKEjtVNu7xPZ5COKw84FNQvofHQ_Umgl6c",
  jwtFromEnv
);
export async function getGoogleSheetsData(range: string) {
  await doc.loadInfo();
  const sheet = doc.sheetsByIndex[0];
  const rows = (await sheet.getRows()).map(row => row.toObject());
  return rows;
}