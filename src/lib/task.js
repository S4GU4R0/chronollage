
/**
 * Description placeholder
 *
 * @class Task
 * @typedef {Task}
 */
class Task {

    /**
     * Creates an instance of Task.
     *
     * @constructor
     * @param {string} title - main text of the task
     * @param { Object } options - optional data to attach to the task
     * @param { string } options.id - task's unique identifier
     * @param { Date } options.dueDate - when the task must be done by
     */
    constructor(title, options) {
        this.title = title
        // copy the options over to the task
        Object.keys(options).forEach(option => this[option] = options[option])
    }
}

export default Task