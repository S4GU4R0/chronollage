const { parsePhoneNumber } = require("libphonenumber-js");
export const formatPhoneNumber = (phoneNumber) => {
    //helper function used below
    try {
      const parsedNumber = parsePhoneNumber("+" + phoneNumber);
      if (parsedNumber.isValid()) {
        return parsedNumber.formatInternational();
      } else {
        return phoneNumber;
      }
    } catch (error) {
      return phoneNumber;
    }
  };