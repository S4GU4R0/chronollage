"use client"
import React, { Suspense } from "react"
import { useLanguage } from "@/hooks";
import { LanguageSwitcher } from "@/components/ui";
import translatedContent from "@/translatedContent.json";

export default function Home() {
  // const { language, switchLanguage } = useLanguage();
  const language = "en"
  return (
      <>
        <header className="">
          <div className="container flex items-center px-4 py-6 mx-auto">
            <a href="/">
              <h1 className="text-6xl font-bold">{translatedContent.title[language]}</h1>
            </a>
          </div>
        </header>
        <div className="flex flex-col items-center mx-2">
          <div className="w-full p-2 ">
            <p className="text-4xl prose">An open source alternative to Skedpal, Moment, and Reclaim. <br /><br />
              Made for people who struggle with executive dysfunction by someone who struggles with executive dysfunction.
            </p>
            <div className="flex flex-wrap p-8">
              <a href="/dashboard" className="m-1 text-3xl shadow-md btn bg-accent text-accent-content shadow-black">Go to App</a>
              <a href="" target="_blank" className="m-1 text-3xl btn bg-primary text-primary-content">GitLab</a>
              <a href="https://opencollective.com/s4gu4r0world/projects/chronollage" target="_blank" className="m-1 text-3xl text-secondary-content bg-secondary btn">OpenCollective</a>
            </div>
          </div>
        </div>
      </>
  )
}

