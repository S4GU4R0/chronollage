import { NextResponse } from "next/server";
import { databases, sdk } from "@/lib/appwrite";

export async function GET(request) {
    const { searchParams } = new URL(request.url);
    const username = searchParams.get('username');
    console.log({ sdk })
    if (username) {
        try {

            let queries = [sdk.Query.equal("username", [username])]
            let found = await databases.listDocuments("chronollage", "users", queries)
            console.log(found)
            return NextResponse.json(found)
        } catch (err) {
            return NextResponse.json({ error: err })
        }
    }
    return NextResponse.json({ message: "nothing happened" })
}