import { google } from 'googleapis';

const GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
const GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET;
const GOOGLE_REDIRECT_URI = `${process.env.NEXTAUTH_URL}/api/google-tasks-auth`;

const oauth2Client = new google.auth.OAuth2(
  GOOGLE_CLIENT_ID,
  GOOGLE_CLIENT_SECRET,
  GOOGLE_REDIRECT_URI
);

export async function GET(request) {
  const { searchParams } = new URL(request.url);
  const code = searchParams.get('code');

  if (code) {
    // Exchange the code for an access token
    const { tokens } = await oauth2Client.getToken(code);
    oauth2Client.setCredentials(tokens);

    // Use the access token to make API requests
    // const tasksClient = google.tasks({ version: 'v1', auth: oauth2Client });
    // const taskLists = await tasksClient.tasklists.list({});

    // Redirect the user back to your application with the access token
    const redirectUrl = new URL(`${process.env.NEXTAUTH_URL}/dashboard/integrations`);
    redirectUrl.searchParams.append('access_token', tokens.access_token);
    return Response.redirect(redirectUrl.toString());
  } else {
    // Generate the authentication URL
    const authUrl = oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: ['https://www.googleapis.com/auth/tasks'],
    });

    // Redirect the user to the authentication URL
    return Response.redirect(authUrl);
  }
}