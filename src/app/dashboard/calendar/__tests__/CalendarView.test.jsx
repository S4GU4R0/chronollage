import { render, screen } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import CalendarView from "@/app/dashboard/calendar/page.tsx"
import dayjs from "dayjs"
let date = dayjs()
// TEST FORMAT:
// ARRANGE
// ACT
// ASSERT

describe("/dashboard/calendar", () => {
    describe("Rendering", () => {
        it("should have a header", () => {
            //ARRANGE
            render(<CalendarView />)
            //ACT
            const header = screen.getByTestId('calendar-header')
            //ASSERT
            expect(header).toBeInTheDocument()
        })
        it("should show zoom in and out buttons", () => {
            //ARRANGE
            render(<CalendarView />)
            //ACT
            const zoomOutButton = screen.getByTestId('zoom-out-button')
            const zoomInButton = screen.getByTestId('zoom-in-button')
            //ASSERT
            expect(zoomOutButton).toBeInTheDocument()
            expect(zoomInButton).toBeInTheDocument()
        })
        it("should show the month view by default", () => {
            //ARRANGE
            render(<CalendarView />)
            //ACT
            const monthView = screen.getByTestId('month-view')
            //ASSERT
            expect(monthView).toBeInTheDocument()
        })
    })
    describe("Behavior", () => {
        it("should allow the user to zoom out in time", () => {
            //ARRANGE
            render(<CalendarView />)
            //ACT
            const zoomOutButton = screen.getByTestId('zoom-out-button')
            userEvent.click(zoomOutButton).then(() => {
                //ASSERT
                const yearView = screen.getByTestId('year-view')
                expect(yearView).toBeInTheDocument()
            }).catch(() => {
                //ASSERT
                const monthView = screen.getByTestId('month-view')
                expect(monthView).not.toBeInTheDocument()

            })
        })
        it("should allow the user to zoom in in time", () => {
            //ARRANGE
            render(<CalendarView />)
            //ACT
            const zoomOutButton = screen.getByTestId('zoom-in-button')
            userEvent.click(zoomOutButton).then(() => {
                //ASSERT
                const weekView = screen.getByTestId('week-view')
                expect(weekView).toBeInTheDocument()
            }).catch(() => {
                //ASSERT
                const monthView = screen.getByTestId('month-view')
                expect(monthView).not.toBeInTheDocument()

            })
        })
    })
})
