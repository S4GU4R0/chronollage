"use client"
import React, { useEffect, useState } from "react"
import { format, subHours, startOfMonth } from 'date-fns';
import {
    MonthlyBody,
    MonthlyDay,
    MonthlyCalendar,
    MonthlyNav,
    DefaultMonthlyEventItem,
    WeeklyCalendar,
    WeeklyContainer,
    WeeklyDays,
    DefaultWeeklyEventItem,
    WeeklyBody
} from '@zach.codes/react-calendar';

export default function CalendarView() {
    return (
        <div data-testid="calendar-view" className="w-full ">
            <div className="w-full mb-16 ">
                {/* <MonthView /> */}
                <WeekView/>
            </div>
        </div>
    )
}

const MonthView = () => {
    let [currentMonth, setCurrentMonth] = useState<Date>(
        startOfMonth(new Date())
    );

    return (
        <MonthlyCalendar
            currentMonth={currentMonth}
            onCurrentMonthChange={date => setCurrentMonth(date)}
        >
            <MonthlyNav />
            <MonthlyBody
                events={[
                    { title: 'Call John', date: subHours(new Date(), 2) },
                    { title: 'Call John', date: subHours(new Date(), 1) },
                    { title: 'Meeting with Bob', date: new Date() },
                ]}
            >
                <MonthlyDay
                    renderDay={data =>
                        data.map((item, index) => (
                            <DefaultMonthlyEventItem
                                key={index}
                                title={item.title}
                                // Format the date here to be in the format you prefer
                                date={format(item.date, 'k:mm')}
                            />
                        ))
                    }
                />
            </MonthlyBody>
        </MonthlyCalendar>
    );
};

const WeekView = () => {
    return (
        <WeeklyCalendar week={new Date()}>
            <WeeklyContainer>
                <WeeklyDays />
                <WeeklyBody
                    events={[{ title: 'Jane doe', date: new Date() }]}
                    renderItem={({ item, showingFullWeek }) => (
                        <DefaultWeeklyEventItem
                            key={item.date.toISOString()}
                            title={item.title}
                            date={
                                showingFullWeek
                                    ? format(item.date, 'MMM do k:mm')
                                    : format(item.date, 'k:mm')
                            }
                        />
                    )}
                />
            </WeeklyContainer>
        </WeeklyCalendar>
    )
}