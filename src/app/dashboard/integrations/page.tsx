"use client"
import React, { useState } from "react"
import ToggleButton from 'react-toggle-button'
import { useRouter } from 'next/navigation'
import IntegrationsDrawer from "@/components/IntegrationsDrawer"

/**
 * The view where the user manages their integrations
*
* @component
* @returns {JSX.Element}
*/
export default function IntegrationsView() {
    const [googleTasksToggle, setGoogleTasksToggle] = useState(false)
    const [googleCalendarToggle, setGoogleCalendarToggle] = useState(false)
    const router = useRouter()
    const [isOpen, setIsOpen] = useState(false);

    const taskManagerIntegrated = false
    const calendarManagerIntegrated = false
    const webhooks = []

    const toggle = () => {
        setIsOpen(!isOpen);
    };

    const toggleGoogleTasksIntegration = (value) => {
        setGoogleTasksToggle(!value)
        if (!value === true) {
            // Initiate the Google authentication flow
            router.push('/api/google-tasks-auth');
        }
    }
    const toggleGoogleCalendarIntegration = (value) => {
        setGoogleCalendarToggle(!value)
    }

    const AddNewIntegrationButton = (
        <div>
            <span>You don't have any integrations of this type.</span>
            <div className="mx-auto mt-8 rounded w-28 h-28 bg-base-200">
                <button onClick={toggle} className="w-full h-full p-0 btn btn-ghost">
                    <i className="mt-2 text-5xl oma oma-add-button"></i>
                    <div className="w-full mb-2 font-bold opacity-50 text-md">Add one here</div>
                </button>
            </div>
        </div>
    )

    const TaskManagers = (
        <section className="card">
            <h2 className="flex items-center justify-between card-title">
                <span>Task Managers</span>
            </h2>
            {taskManagerIntegrated ? <ul>
                <li className="flex items-center">
                    <ToggleButton
                        value={googleTasksToggle || false}
                        onToggle={toggleGoogleTasksIntegration}
                        className="border"
                        inactiveLabel={<i className="text-xl oma oma-cross-mark" />}
                        activeLabel={<i className="text-xl oma oma-check-mark" />}

                    />
                    <h3 className="my-0 ml-2">Google Tasks</h3>
                </li>
            </ul> : (
                <> {AddNewIntegrationButton} </>
            )
            }
        </section>
    )



    const CalendarManagers = (
        <section className="card">
            <h2 className="card-title">Calendars</h2>
            {calendarManagerIntegrated ? <ul>
                <li className="flex">
                    <ToggleButton
                        value={googleCalendarToggle || false}
                        onToggle={toggleGoogleCalendarIntegration}
                        inactiveLabel={<i className="text-xl oma oma-cross-mark" />}
                        activeLabel={<i className="text-xl oma oma-check-mark" />}
                    />
                    <h3 className="my-0 ml-2">Google Calendar</h3>
                </li>
            </ul> : (
                <> {AddNewIntegrationButton} </>
            )}
        </section>
    )

    const WebhookManager = (
        <section className="card">
            <h2 className="card-title">Webhooks</h2>
            {webhooks.length === 0 && <> {AddNewIntegrationButton} </>}
        </section>
    )

    return (
        <div className="mb-16 prose ">
            <h1 className="sr-only">Integration Settings</h1>
            <IntegrationsDrawer {...{ toggle, isOpen, router }} />

            {TaskManagers}
            {CalendarManagers}
            {WebhookManager}
        </div>
    )
}