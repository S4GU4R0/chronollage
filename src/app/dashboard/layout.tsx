"use client"
import React from "react";
import Navbar from "@/components/Navbar"
export default function AppHomeLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <div data-theme="light" className="w-screen p-0 m-0">
            <div className="w-screen min-h-screen p-0 m-0">
                <Navbar />
                <div className="px-6">{children}</div>
            </div>
        </div>
    );
}
