"use client"
import React, { useEffect } from "react"
import { redirect } from 'next/navigation'

export default function AccountView() {
    useEffect(() => {
        redirect('/login')
    }, [])
    return (
        <div>Account view</div>
    )
}