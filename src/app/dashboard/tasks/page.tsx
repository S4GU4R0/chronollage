"use client"
import TaskEditDrawer from "@/components/TaskEditDrawer"
import React, { useState, useEffect } from "react"
import {
    LeadingActions,
    SwipeableList,
    SwipeableListItem,
    SwipeAction,
    TrailingActions,
} from 'react-swipeable-list';
import 'react-swipeable-list/dist/styles.css';

/**
 * For viewing, managaging, grouping, and filtering lists of tasks.
 *
 * @component 
 * @returns {JSX.Element} the rendered list of tasks with controls for managing the items
 * 
 * @example
 * // No need to invoke this component. it's automatically invoked by Next.js
 */
export default function TasksView() {
    const [isOpen, setIsOpen] = useState(false);

    const sample = [{ title: 1, id: 1, notes: "", source: "google-tasks" }, { title: 2, id: 2, notes: "", source: "habitica" }, { title: 3, id: 3, notes: "", source: "ios-reminders" }]
    const groups = [{ title: "dependencies", id: 1 }, { title: "due date", id: 2 }, { title: "source", id: 3 }]
    const [currentGroup, setCurrentGroup] = useState(null);
    const filters = [{ title: "filter 1", id: 1 }, { title: "filter 2", id: 2 }, { title: "filter 3", id: 3 }]
    const [sources, setSources] = useState([])

    const [currentTask, setCurrentTask] = useState(null);

    const toggle = () => {
        setIsOpen(!isOpen);
    };
    const openEditMenu = (id) => {
        toggle()
        setCurrentTask(id)
    }
    const deleteTask = (id) => {
        console.log("deleting ", id)
    }

    const computeSources = () => {
        let newSources = new Map()

        //grab sources from tasks and remove duplicates
        sample.forEach(task => newSources.set(task.source, true))
        newSources = Array.from(newSources, ([name]) => ({ title: name, id: name }))
        setSources(newSources)
    }

    useEffect(computeSources, [])

    const GroupsBadges = groups.filter((group) => {
        if (currentGroup !== null) {
            return group.id === currentGroup
        } else {
            return true
        }
    }).map(group => <button
        key={`group-${group.id}`}
        onClick={() => {
            if (currentGroup === group.id) {
                setCurrentGroup(null)
            } else {
                setCurrentGroup(group.id)
            }
        }} className="flex items-center mr-2 badge badge-outline"><span>{group.title}</span> {currentGroup === group.id && <i className="oma oma-close"></i>}
    </button>)

    const FiltersBadges = filters.map(filter => <button
        key={`filter-${filter.id}`}
        onClick={() => setCurrentGroup(filter.id)}
        className="mr-2 badge badge-outline">
        {filter.title}
    </button>)



    const leadingActions = (task) => (
        <LeadingActions>
            <SwipeAction
                onClick={() => openEditMenu(task.id)}
                className="flex flex-col justify-center h-full bg-warning"
            >
                <i className="mx-auto text-6xl oma oma-edit"></i>
            </SwipeAction>
        </LeadingActions>
    );

    const trailingActions = (task) => (
        <TrailingActions>
            <SwipeAction
                // destructive={true}
                onClick={() => deleteTask(task.id)}
                className="flex flex-col justify-center h-full bg-error"
            >
                <i className="mx-auto text-6xl oma oma-delete"></i>
            </SwipeAction>
        </TrailingActions>
    );

    return (
        <div>
            <div className="p-4 mb-8 border-b border-black">
                <div className="flex flex-wrap items-center flex-start">
                    <span className="mr-2">group by:</span>
                    {GroupsBadges}
                </div>
                <div className="flex flex-wrap items-center flex-start">
                    <span className="mr-2">filter by:</span>
                    {FiltersBadges}
                </div>
            </div>
            <TaskEditDrawer {...{ isOpen, toggle, task: currentTask }} />
            {currentGroup === null && <SwipeableList
                style={{ width: "100%" }}>
                {sample.map(task => {
                    return (<SwipeableListItem

                        leadingActions={leadingActions(task)}
                        trailingActions={trailingActions(task)}
                        className="card card-outline"
                    ><TaskCard {...{ openEditMenu, deleteTask, task }} /></SwipeableListItem>)
                })}
            </SwipeableList>}

            {/* BY GROUPS */}
            {currentGroup === 3 &&
                <ul>
                    {sources.map(source => {
                        return (<TaskCard {...{ openEditMenu, deleteTask, task: source }} />)
                    })}

                </ul>
            }

        </div>
    )
}

/**
 * Description placeholder
 *
 * @param {{ openEditMenu: any; deleteTask: any; task: any; }} param0
 * @param {*} param0.openEditMenu
 * @param {*} param0.deleteTask
 * @param {*} param0.task
 * @returns {*}
 */
function TaskCard({ openEditMenu, deleteTask, task }) {
    console.log({ task })
    return (
        <div className="mb-2 border-black list-style-none">
            <div className="shadow-xl card w-96 bg-base-100">
                <div className="card-body">
                    <h2 className="card-title">{task.title}</h2>
                    <p className="card-body">{task.notes}</p>
                </div>
            </div>
        </div>
    )
}