import React, { Suspense } from "react";
import type { Metadata } from "next";
import "./globals.css";
import "react-day-picker/dist/style.css";

export const metadata: Metadata = {
  metadataBase: new URL('https://chronollage.org'),
  title: "Chronollage",
  description: "PLacing tasks in time.",
  keywords: ['schedule', 'open source', 'calendar', 'google calendar'],
  openGraph: {
    title: "Chronollage",
    description: "Placing tasks in time.",
    url: "https://chronollage.org",
    type: "website",
    images: [
      {
        url: "/og-image_large.png",
        width: 1200,
        height: 630,
        alt: "Chronollage",
      },
    ],
  },
  twitter: {
    cardType: "summary",
    title: "Chronollage",
    description: "Support our mission to assist families affected by the genocide in Gaza. Learn more about the Chronollage.",
    image: "/og-image_large.png",
    imageAlt: "Chronollage",
  },
};


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" data-theme="light" className="p-0 m-0">
      <body className="flex flex-col justify-between w-screen min-h-screen p-0 m-0"><div>{children}</div>
        <footer className="sticky w-screen p-0 m-0 text-neutral-content bg-neutral neutral-content">
          <div className="container px-4 py-6 mx-auto">
            <p>&copy; 2024 Chronollage. All rights reserved. Built by <a className="underline" href="https://s4gu4r0.world" target="_blank" rel="noopener noreferrer">S4GU4R0</a>.</p>
          </div>
        </footer>
      </body>
    </html>


  );
}
