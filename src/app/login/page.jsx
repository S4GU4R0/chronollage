"use client"
import React from "react"
import { useForm } from "react-hook-form";
import * as yup from "yup";
import YupPassword from 'yup-password'
import { yupResolver } from "@hookform/resolvers/yup";

YupPassword(yup) // extend yup

const schema = yup.object().shape({
    username: yup.string()
        .min(6, "Username must be at least 6 characters")
        .max(20, "Username must not exceed 20 characters")
        .test("Unique Username", "Username already in use", function (value) {
            return new Promise((resolve, reject) => {
                if (value.length > 5 && value.length < 20) {
                    fetch(`/api/login?username=${value}`).then((res) => {
                        return res.json()
                    }).then((res) => {
                        console.log({ res })
                        if (res.total === 0) {
                            resolve(true)
                        } else {
                            resolve(false)
                        }
                    }).catch(error => {
                        console.log(error)
                    })
                } else {
                    resolve(true)
                }
            })
        })
        .required("Username is required"),
    password: yup.string().password().required(),
    passwordConfirmation: yup.string()
        .oneOf([yup.ref('password'), null], 'Passwords must match')
});

export default function LoginPage() {
    const { register, handleSubmit, formState: { errors }, reset } = useForm({
        mode: "onChange",
        resolver: yupResolver(schema),
    });
    const onSubmitHandler = (data) => {
        console.log({ data });
        reset();
        
    };
    return (
        <div className="p-6 prose">
            <h1 className="text-center">Login or Register</h1>
            <form onSubmit={handleSubmit(onSubmitHandler)} className="flex flex-col items-center mx-8">
                <h2 className="self-start p-0 m-0">Username</h2>
                <p className="w-full p-0 m-0 font-bold text-left text-error">{errors.username?.message}</p>
                <label className="flex items-center gap-2 input input-bordered">
                    <i className="text-2xl oma oma-bust-in-silhouette"></i>
                    <input {...register("username")}
                        type="text"
                        className="grow"
                        placeholder="Username*"
                        autcomplete="username" />
                </label>
                <h2 className="self-start mt-4 mb-0 ml-0 mr-0">Password</h2>
                <p className="w-full p-0 m-0 font-bold text-left text-error">{errors.password?.message}</p>
                <label className="flex items-center gap-2 input input-bordered">
                    <i className="text-2xl oma oma-key"></i>
                    <input {...register("password")}
                        placeholder="password*"
                        type="password"
                        className="grow"
                        autoComplete="new-password"
                        required
                    />
                </label>
                <h2 className="self-start mt-4 mb-0 ml-0 mr-0">Confirm Password</h2>
                <p className="w-full p-0 m-0 font-bold text-left text-error">{errors.passwordConfirmation?.message}</p>
                <label className="flex items-center gap-2 input input-bordered">
                    <i className="text-2xl oma oma-key"></i>
                    <input {...register("passwordConfirmation")}
                        placeholder="password*"
                        type="password"
                        className="grow"
                        autoComplete="new-password"
                        required
                    />
                </label>
                <button className="mt-8 btn-lg btn btn-primary" type="submit">Submit</button>
            </form>
        </div>
    )
}