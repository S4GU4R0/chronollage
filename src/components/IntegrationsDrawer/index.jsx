"use client"
import React, { useState } from 'react';
import Drawer from 'react-drag-drawer'
import translatedContent from "@/translatedContent.json";

const IntegrationsDrawer = ({ isOpen, toggle, router }) => {

    return (


        <Drawer
            open={isOpen}
            onRequestClose={toggle}
            modalElementClass="drawer-contents"
            containerElementClass="drawer"
        >
            <div className="p-6 prose">
                <h1>Pick an app</h1>
                <ul className="flex flex-wrap w-full p-0 list-none">
                    <li className="w-24 h-24 p-0 m-2">
                        <button onClick={() => router.push('/api/google-tasks-auth')} className="w-full h-full p-2 m-2 btn btn-square">
                            <img className="w-10 h-10 p-0 m-0" src="https://static.wikia.nocookie.net/logopedia/images/5/5b/Google_Tasks_2021.svg" />
                            <div>Google Tasks</div>
                        </button>
                    </li>
                    <li className="w-24 h-24 p-0 m-2">
                        <button onClick={() => router.push('/api/google-calendar-auth')} className="w-full h-full p-2 m-2 btn btn-square">
                            <img className="w-10 h-10 p-0 m-0" src="https://raw.githubusercontent.com/gilbarbara/logos/29e8719bf78915c7a82a26a6c203f53c4cb8fff2/logos/google-calendar.svg" />
                            <div>Google Calendar</div>
                        </button>
                    </li>
                </ul>
            </div>

        </Drawer>

    );
};

export default IntegrationsDrawer;

