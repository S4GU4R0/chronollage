"use client"
import React, { useState } from 'react';
import Drawer from 'react-drag-drawer'
import translatedContent from "@/translatedContent.json";

const Navbar = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggleMenu = () => {
        setIsOpen(!isOpen);
    };



    const SearchBar = (<div className="hidden lg:flex">
        <input
            className="inline-block px-4 py-3 text-sm font-semibold border border-r-0 rounded-l placeholder-base-content text-base-content bg-base-100 focus:outline-primary"
            placeholder="Search"
        />
        <button className="px-2 border border-l-0 rounded-r light:border-black dark:border-neutral">
            <i className="text-2xl oma oma-magnifying-glass-tilted-left"></i>
            <span className="sr-only">Submit search</span>
        </button>
    </div>)

    return (

        <nav data-theme="light" className="px-10 py-2 bg-transparent">
            <div className="flex items-center justify-between">
                <a className="text-2xl leading-none text-base-content" href="#">
                    <i className="text-7xl oma oma-mantelpiece-clock"></i>
                </a>
                {SearchBar}
                <div className="">
                    <button
                        className=""
                        onClick={toggleMenu}
                    >
                        <i className="text-4xl oma oma-hamburger-menu"></i>
                    </button>
                </div>

            </div>
            <Drawer
                open={isOpen}
                onRequestClose={toggleMenu}
                modalElementClass="drawer-contents"
                containerElementClass="drawer"
            >
                <nav className="flex flex-col w-full h-full px-6 py-6 overflow-y-auto bg-base-100 ">
                    <div className="flex items-center justify-end mb-8">
                        <button className="navbar-close" onClick={toggleMenu}>
                            <i className="text-2xl oma oma-close"></i>
                            {/* TODO: need icons that work with dark backgrounds */}
                        </button>
                    </div>
                    <div className="flex flex-col justify-between h-full">
                        <div>
                            <h2 className="text-xl font-bold text-base-content">{translatedContent.app.sidebar.views["en"]}</h2>
                            <ul className="flex flex-wrap">
                                <li>
                                    <a href="/dashboard/plan" className="w-24 h-24 m-1 text-xl btn btn-square bg-accent text-accent-content"><div className="flex flex-col justify-between w-full h-full p-2">
                                        <i className="text-5xl oma oma-compass"></i>
                                        <span className="text-left uppercase">{translatedContent.app.sidebar.plan["en"]}</span>
                                    </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="/dashboard/calendar" className="w-24 h-24 m-1 text-xl btn btn-square bg-secondary text-secondary-content"><div className="flex flex-col justify-between w-full h-full p-2">
                                        <i className="text-5xl oma oma-calendar"></i>
                                        <span className="text-left uppercase">{translatedContent.app.sidebar.calendar["en"]}</span>
                                    </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="/dashboard/tasks" className="w-24 h-24 m-1 text-xl btn btn-square bg-primary text-primary-content"><div className="flex flex-col justify-between w-full h-full p-2">
                                        <i className="text-5xl oma oma-check-mark"></i>
                                        <span className="text-left uppercase">{translatedContent.app.sidebar.tasks["en"]}</span>
                                    </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div>
                            <h2 className="text-xl font-bold text-base-content">{translatedContent.app.sidebar.controls["en"]}</h2>
                            <ul className="flex flex-col">
                                <li><a href="/dashboard/integrations" className="m-1 btn btn-md bg-accent text-accent-content"><i className="text-2xl oma oma-gear"></i> Integrations</a></li>
                                <li><a href="/dashboard/account" className="m-1 btn btn-md btn-outline text-base-content"><i className="text-2xl oma oma-bust-in-silhouette"></i> Account</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className="mt-auto">
                        <p className="mt-6 mb-4 text-sm text-center text-base-content">
                            <span>{translatedContent.copyright["en"]}</span>
                        </p>
                    </div>
                </nav>

            </Drawer>
        </nav>
    );
};

export default Navbar;