"use client"
import React, { useState } from 'react';
import Drawer from 'react-drag-drawer'
import translatedContent from "@/translatedContent.json";

const TaskEditDrawer = ({ isOpen, toggle, task }) => {





    return (


        <Drawer
            open={isOpen}
            onRequestClose={toggle}
            modalElementClass="drawer-contents"
            containerElementClass="drawer"
        >
            <div className="">
                <h1>Editing {task}</h1>
                <form>
                    <input type="text" />
                </form>
            </div>

        </Drawer>

    );
};

export default TaskEditDrawer;

